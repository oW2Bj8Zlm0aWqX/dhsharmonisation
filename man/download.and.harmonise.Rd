\name{download.and.harmonise}
\alias{download.and.harmonise}
\title{
  Download and harmonise
}
\description{
  Collect and harmonise data from Demographics and Health Surveys and MICS
}
\usage{

download.and.harmonise(dhs.user, dhs.password =
getPass::getPass("Please enter your DHS password:"), mics.user, mics.password =
getPass::getPass("Please enter your MICS password:"), log.filename=
"living-conditions.log", reporters = FALSE, vars.to.keep =
c("m49.region", "country.code.ISO.3166.alpha.3", "version", "RegionID",
  "superClusterID", "ClusterID", "HouseholdID", "year.of.interview",
  "month.of.interview", "severe.education.deprivation",
  "severe.food.deprivation", "severe.water.deprivation",
  "severe.sanitation.deprivation", "severe.shelter.deprivation",
  "has.electricity", "age", "sex", "sample.weight", "lon", "lat"),
variables.to.inherit = c("severe.education.deprivation", "education",
"education.in.years", "Religion", "Age.at.first.cohabitation", "caste"),
  countries = NULL,
  waves = NULL,
  file.types.to.download = c("PR", "GE", "IR", "KR", "MR"),
  max.file.size.for.parallelisation = NULL,
  make.pdf = FALSE,
  directory = "global-living-conditions",
  variable.packages = NULL,
  satellite.images = FALSE,
  precipitation = FALSE,
  temperature = FALSE,
  malaria = FALSE,
  superclusters = TRUE,
  check.dhs.for.more.data = TRUE,
  qog.vars = c("wbgi_gee", "wdi_gnicapatlcur"),
  temperature.file.path = NULL,
  qog.file.path = NULL,
  use.renv = FALSE
)
}
\arguments{
  \item{dhs.user}{
    The username used to authenticate against the DHS web server.
  }
  \item{dhs.password}{
    The password used to authenticate against the DHS web server.
  }
  \item{mics.user}{
    The username used to authenticate against the MICS web server.
  }
  \item{mics.password}{
    The password used to authenticate against the MICS web server.
  }
  \item{log.filename}{
    A name of the file which the program will log events to. If the file
    does not exist, it will be created.
  }
    \item{reporters}{
    Boolean, if TRUE, download the country reports files. If FALSE, do not
    download the country reports files.
    }
  \item{vars.to.keep}{
    A character vector of variables names to include.
  }
  \item{countries}{
    (Optional) A character vector of country names to include. The names
    in this vector must match the long country names used by DHS on the
    website.

    If NULL, then all countries that the user has access to will be included.
  }
  \item{variables.to.inherit}{
    (Optional) A character vector of variable names that the children
  will inherit from the parents. Useful for analyses of outcomes of the
  children where attributes of the parents is used as explanatory factors.

    If NULL, then no attributes will be inherited.
  }
  \item{waves}{
    (Optional) A list of character vector(s) with waves to include. The names
    in this list must match the long country names used by DHS on the
    website. The elements in the character vector(s) corresponds to the
    two letter codes ascribed to all DHS waves, e.g. "3A", "62".

    If NULL, then all countries that the user has access to will be
    included.

    NB: If waves is not NULL, then it is not necessary to specify
  `countries`. 
}
\item{file.types.to.download}{
  If you want to use only data from the a particular type of files, set
  this parameter, e.g. `file.types.to.download` = "PR".
}
\item{max.file.size.for.parallelisation}{
  On Linux and OS X operating systems, ignore this parameter. If you
  have a machine running MS Windows with a small amount of RAM, set this
  parameter to something less than 300000000 to avoid running of of RAM.
}
\item{make.pdf}{
  Set to FALSE to inhibit create of a PDF documenting the
  harmonisation process. Mostly useful on systems without a proper
  xelatex installation.
  }
  \item{directory}{
    The directory where the end result will be saved. Currently,
    temporary files used in the process are not deleted.
  }
  \item{variable.packages}{
    (Optional) A character vector of variable packages to include. The names
    in this vector must match a csv-file in `inst`. Currently supported
  names: "malaria" and "weight".

    If NULL, then no additional variable packages are envoked.
  }
  \item{satellite.images}{
    Boolean, if TRUE, use google static map API to download satellite
  images for all clusters. Access to google static map API requires a
  developer account on google. See the code the details on
  authentication.
  }
  \item{precipitation}{
    Boolean, if TRUE, use urs.earthdata.nasa.gov to aquire data on
  precipitation for all clusters. Access to urs.earthdata.nasa.gov
  requires registration. See the code the details on
  authentication.
  }
  \item{temperature}{
    Boolean, if TRUE, use berkeleyearth.lbl.gov to aquire data on
  temperature for all clusters.
  }
  \item{malaria}{
    Boolean, if TRUE, use the package "malariaAtlas" to aquire data on
  malaria incidence for all clusters. See the code for details on how to
  specify which variables to add from that source.
}
\item{superclusters}{
  Boolean, if TRUE, automatically partition countries with more than
  one survey wave with GIS data. This is relatively computationally
  intense so setting this to false will speed up the harmonisation.
}
\item{check.dhs.for.more.data}{
  Boolean, if TRUE, the default value, check if there are new file
  to download, which can happen for two reasons: (1) new data published
  by DHS, (2) the user expands the file.types.to.download to include
  more types. For most users leaving this to the default value is safe.

  The main point of this flag is saving time when re-running the
  harmonisation with different parameters.
}
\item{qog.vars}{
  A character vector of variables from the Quality of Government
  database to include. Defaults to c("wbgi_gee") which measures
  "Government Effectiveness, Estimate".
}
\item{qog.file.path}{
  A string that specifies the path to a local copy of the Quality
  of Government database. Defaults to NULL. This parameter is useful
  when the user wants to keep several independent data directories,
  without having to re-download the Quality of Government database.
  The version of the QoG database used by default is about 200 MB.
  The source for the qog file is
  \url{https://www.qogdata.pol.gu.se/data/qog_std_ts_jan21.sav}.
  If you want to use this parameter, download that file to your home
  directory and set qog.file.path to paste(normalizePath("~"),
  "qog_std_ts_jan21.sav", sep = "/")
}
\item{temperature.file.path}{
  A string that specifies the path to a local copy of the Temperature
  database. Defaults to NULL. This parameter is useful
  when the user wants to keep several independent data directories,
  without having to re-download the Temperature database.
  The version of the Temperature database used by default is about
  200 MB. The source for the qog file is
  \url{http://berkeleyearth.lbl.gov/auto/Global/Gridded/Complete_TAVG_LatLong1.nc}.
}

\item{use.renv}{
  Boolean, if TRUE, create a virtual environment and install packages
  into that environment. if FALSE, the user is responsible for
  installing the required packages.
}

}
\value{
  NULL.
}
\author{
  Hans Ekbrand
}

\examples{
     ## Not run:
  ## library(globallivingconditions)
  ## my.df <- download.and.harmonise(dhs.user='hans.ekbrand@gu.se',
  ## dhs.password='r3al.passw0rd')
  ## only include datasets from Bangladesh and India
  ## my.subset.df <- download.and.harmonise(dhs.user='hans.ekbrand@gu.se',
  ## dhs.password='r3al.passw0rd', countries = c("Tanzania", "Mozambique"),
  ## waves = list("Mozambique" = c("7A"), "Tanzania" = "7I")
  ## use a pre-downloaded file for Quality of Government database
  ## my.df <- download.and.harmonise(dhs.user='hans.ekbrand@gu.se',
  ## dhs.password='r3al.passw0rd',
## qog.file.path=paste(normalizePath("~"), "qog_std_ts_jan21.sav", sep = "/")
     ## End(Not run)
}
