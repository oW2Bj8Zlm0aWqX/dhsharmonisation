## Survey Contents

# The contents of the tables found online via https://mics.unicef.org/contents-by-survey have all been copied and pasted into the MICS_Contents-by-Survey.ods. Each separate table is its own Worksheet.

import::from(data.table, data.table)
import::from(rio, import)
import::from(readODS, list_ods_sheets)


# get the filename for the MICS Content by Survey ODS (OpenDocument spreadhseet) file
mics_surveys_file <- system.file("MICS_survey", "MICS_Contents-by-Survey.ods", package = "globallivingconditions")

# list the sheet names
mics_surveys_sheets <- list_ods_sheets(mics_surveys_file)


## MICS 1
# https://mics.unicef.org/contents-by-survey#MICS1

# Contents of questionnaires

# import the sheet named MICS1
mics1 <- import(mics_surveys_file, setclass = "data.table", which = "MICS1")

# remove the unnecessary rows
mics1 <- mics1[-c(64:nrow(mics1)), ]

# save(mics1, file = "./inst/MICS_survey/MICS1.rda")



## MICS 2
# https://mics.unicef.org/contents-by-survey#MICS2

# Contents of household questionnaires
# import the sheet named MICS2_House
mics2_house <- import(mics_surveys_file, setclass = "data.table", which = "MICS2_House")

mics2_house <- mics2_house[-c(67:nrow(mics2_house)), ]

# save(mics2_house, file = "./inst/MICS_survey/MICS2_House.rda")


# Contents of women's questionnaire
# import the sheet named MICS2_Women

mics2_women <- import(mics_surveys_file, setclass = "data.table", which = "MICS2_Women")

mics2_women <- mics2_women[-c(66:nrow(mics2_women)), ]

# save(mics2_women, file = "./inst/MICS_survey/MICS2_Women.rda")


# Contents of under-5 questionnaire
# import the sheet named MICS2_Under5

mics2_under5 <- import(mics_surveys_file, setclass = "data.table", which = "MICS2_Under5")

mics2_under5 <- mics2_under5[-c(66:nrow(mics2_under5)), ]

# save(mics2_under5, file = "./inst/MICS_survey/MICS2_Under5.rda")



## MICS 3
# https://mics.unicef.org/contents-by-survey#MICS3

# Contents of household questionnaires
# import the sheet named MICS3_House
mics3_house <- import(mics_surveys_file, setclass = "data.table", which = "MICS3_House")

mics3_house <- mics3_house[-c(54:nrow(mics3_house)), ]

# save(mics3_house, file = "./inst/MICS_survey/MICS3_House.rda")



# Contents of women's questionnaire
# import the sheet named MICS3_Women

mics3_women <- import(mics_surveys_file, setclass = "data.table", which = "MICS3_Women")

mics3_women <- mics3_women[-c(54:nrow(mics3_women)), ]

# save(mics3_women, file = "./inst/MICS_survey/MICS3_Women.rda")


# Contents of under-5 questionnaire
# import the sheet named MICS3_Under5

mics3_under5 <- import(mics_surveys_file, setclass = "data.table", which = "MICS3_Under5")

mics3_under5 <- mics3_under5[-c(54:nrow(mics3_under5)), ]

# save(mics3_under5, file = "./inst/MICS_survey/MICS3_Under5.rda")



## MICS 4
# https://mics.unicef.org/contents-by-survey#MICS4

# Contents of household questionnaires
# import the sheet named MICS4_House
mics4_house <- import(mics_surveys_file, setclass = "data.table", which = "MICS4_House")

mics4_house <- mics4_house[-c(61:nrow(mics4_house)), ]

# save(mics4_house, file = "./inst/MICS_survey/MICS4_House.rda")



# Contents of women's questionnaire
# import the sheet named MICS4_Women

mics4_women <- import(mics_surveys_file, setclass = "data.table", which = "MICS4_Women")

mics4_women <- mics4_women[-c(61:nrow(mics4_women)), ]

# save(mics4_women, file = "./inst/MICS_survey/MICS4_Women.rda")



# Contents of under-5 questionnaire
# import the sheet named MICS4_Under5

mics4_under5 <- import(mics_surveys_file, setclass = "data.table", which = "MICS4_Under5")

mics4_under5 <- mics4_under5[-c(61:nrow(mics4_under5)), ]

# save(mics4_under5, file = "./inst/MICS_survey/MICS4_Under5.rda")



# Contents of men's questionnaire
# import the sheet named MICS4_Men

mics4_men <- import(mics_surveys_file, setclass = "data.table", which = "MICS4_Men")

mics4_men <- mics4_men[-c(61:nrow(mics4_men)), ]

# save(mics4_men, file = "./inst/MICS_survey/MICS4_Men.rda")



## MICS 5
# https://mics.unicef.org/contents-by-survey#MICS5

# Contents of household questionnaires
# import the sheet named MICS5_House
mics5_house <- import(mics_surveys_file, setclass = "data.table", which = "MICS5_House")

mics5_house <- mics5_house[-c(52:nrow(mics5_house)), ]

# save(mics5_house, file = "./inst/MICS_survey/MICS5_House.rda")


# Contents of women's questionnaire
# import the sheet named MICS5_Women

mics5_women <- import(mics_surveys_file, setclass = "data.table", which = "MICS5_Women")

mics5_women <- mics5_women[-c(53:nrow(mics5_women)), ]

# save(mics5_women, file = "./inst/MICS_survey/MICS5_Women.rda")



# Contents of under-5 questionnaire
# import the sheet named MICS5_Under5

mics5_under5 <- import(mics_surveys_file, setclass = "data.table", which = "MICS5_Under5")

mics5_under5 <- mics5_under5[-c(53:nrow(mics5_under5)), ]

# save(mics5_under5, file = "./inst/MICS_survey/MICS5_Under5.rda")



# Contents of men's questionnaire
# import the sheet named MICS5_Men

mics5_men <- import(mics_surveys_file, setclass = "data.table", which = "MICS5_Men")

mics5_men <- mics5_men[-c(53:nrow(mics5_men)), ]

# save(mics5_men, file = "./inst/MICS_survey/MICS5_Men.rda")
