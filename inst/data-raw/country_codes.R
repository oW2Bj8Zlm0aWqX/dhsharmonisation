# This R script provides the ISO 3166 version alpha 3 country codes for use in identifying the countries

# Table source: https://unstats.un.org/unsd/methodology/m49/overview/ | Standard country or area codes for statistical use (M49)


import::from(data.table, fread, setnames, setkey, copy, ":=")

# read in the csv file
country_codes <- data.table::fread("./inst/ISO_country_codes/UNSD_Methodology.csv",  sep = ",", colClasses = "character", fill = TRUE)

# change the column names
data.table::setnames(country_codes, c("Country or Area", "ISO-alpha3 Code", "M49 Code"), c("Country_or_Area", "ISO_alpha3_Code" ,"M49_Code"))


country_codes[, ISO_alpha3_M49_Code := paste0(ISO_alpha3_Code, M49_Code)]


country_codes_subset <- country_codes[, .(Country_or_Area, ISO_alpha3_Code, M49_Code, ISO_alpha3_M49_Code)]


data.table::setkey(country_codes_subset, Country_or_Area)


ISO_country_codes <- data.table::copy(country_codes_subset)


## the ISO country codes are saved within the "inst/ISO_country_codes" folder
save(ISO_country_codes, file = "./inst/ISO_country_codes/ISO_country_codes.rda")
